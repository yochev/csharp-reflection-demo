﻿using DemoReflection.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoReflection
{
    class Program
    {
        static List<Song> PopulateSongs()
        {
            List<Song> result = new List<Song>();
            for (int i = 0; i < 10; i++)
            {
                Song song = new Song()
                {
                    Id = i,
                    Title = $"Song {i}",
                    Artist = $"Artist {i}",
                    Year = 2000 + i
                };
                result.Add(song);
            }

            return result;
        }
        static List<Award> PopulateAwards()
        {
            List<Award> result = new List<Award>();
            for (int i = 0; i < 3; i++)
            {
                Award award = new Award()
                {
                    Id = i,
                    Name = $"Award {i}",
                    SongId = 1,
                    DateOfAward = DateTime.Today.AddDays(i)
                };
                result.Add(award);
            }

            return result;
        }

        static void Main(string[] args)
        {
            //List<Song> songs = PopulateSongs();
            //Controller<Song> songsController = new Controller<Song>(songs);
            //songsController.GetAll();

            List<Award> awards = PopulateAwards();
            Controller<Award> awardsController = new Controller<Award>(awards);
            awardsController.GetAll();
            //awardsController.Get(5);
            awardsController.Insert();
            awardsController.GetAll();

            Console.ReadKey();

        }
    }
}
