﻿using DemoReflection.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoReflection
{
    class Controller<T> where T : BaseEntity, new()
    {
        private List<T> items;
        public Controller(List<T> items)
        {
            this.items = items;
        }

        public void GetAll()
        {
            Console.WriteLine(typeof(T).Name + "s");
            foreach (var item in items)
            {
                foreach (var pi in item.GetType().GetProperties())
                {
                    Console.WriteLine($"{pi.Name}: {pi.GetValue(item)}");
                }
                Console.WriteLine("========================");
            }
        }

        public void Get(int id)
        {
            T item = items.SingleOrDefault(i => i.Id == id);
            foreach (var pi in item.GetType().GetProperties())
            {
                Console.WriteLine($"{pi.Name}: {pi.GetValue(item)}");
            }
            Console.WriteLine("========================");
        }
        public void Insert()
        {
            T item = new T();
            foreach (var pi in item.GetType().GetProperties())
            {
                Console.Write($"{pi.Name}: ");
                pi.SetValue(item, Convert.ChangeType(Console.ReadLine(), pi.PropertyType));
            }

            items.Add(item);
        }
        public void Update() { }
    }
}
