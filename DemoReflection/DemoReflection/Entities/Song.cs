﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoReflection.Entities
{
    class Song : BaseEntity
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Artist { get; set; }
    }
}
