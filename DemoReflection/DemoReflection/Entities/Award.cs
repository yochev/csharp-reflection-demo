﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoReflection.Entities
{
    class Award : BaseEntity
    {
        public string Name { get; set; }
        public int SongId { get; set; }
        public DateTime DateOfAward { get; set; }
    }
}
